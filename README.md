Made in request of a friend for restarting his pc. Ik coud have done it with discord.Client but i want to have a bit more experience with .ext.Commands so ye.

### Remember to add the required things and install required modules

### __Required Modules__
`py -m pip install -r requirements.txt`

### __Required Things__
- [PowerShell Core](https://github.com/PowerShell/PowerShell/releases/latest) // Just incase you don't have this installed.
- Each memberID in `.member` file needs to be in newlines
- *in .env file*
    - Token: Bot Token
    - prefix: command prefix
    - status: Bot's Game/Activity/Status you want it to have

### __Things you might want to do__
- You might be some rando that uses linux. Go change the subprocess command to a restart command of the cli you use.
- If you're using windows go ahead and do what i have written in startup.ps1 and copy the file to `shell:startup` (type that in the run box win+r and enter)
> also i recommend using PowerShell Core to not bother with errors the old default PowerShell spit out.