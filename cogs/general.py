from discord.ext import commands
import subprocess


def authorizedmems():
    with open('./.members', 'r') as file:
        content = file.read()

    return [int(member) for member in content.split('\n')]


class ExysCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.members = authorizedmems()

    @commands.command(
        name='reboot',
        aliases=['rb']
    )
    async def reboot(self, ctx):
        if ctx.author.id in self.members:
            subprocess.Popen(
                [
                    'pwsh',
                    '-Command',
                    'Restart-Computer -Force'
                ],
                creationflags=subprocess.CREATE_NEW_CONSOLE
            )


def setup(bot):
    bot.add_cog(ExysCommands(bot))
