from discord.ext import commands
import discord
from glob import glob
from dotenv import load_dotenv
import os

COGS = [path.split('\\')[-1][:-3] for path in glob('./cogs/*py')]


class BotBase(commands.Bot):
    def __init__(self):
        load_dotenv()
        self.prefix = os.getenv('prefix')
        self.token = os.getenv('token')
        super().__init__(
            command_prefix=self.prefix,
            help_command=None,
            intents=discord.Intents.default()
        )

    def run(self):
        for COG in COGS:
            self.load_extension(f'cogs.{COG}')
        print('Bot is starting....')
        super().run(self.token)

    async def on_ready(self):
        for i, guild in enumerate(self.guilds):
            print(f"{i+1}: {guild} - {guild.id}")

    async def on_connect(self):
        game = discord.Game(os.getenv('status'))
        await self.change_presence(activity=game)


if __name__ == "__main__":
    bot = BotBase()
    bot.run()
